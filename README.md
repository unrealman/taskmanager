# README #

### What is this repository for? ###

Приложение написанно в целях изучения srping boot, spring Security,  websocket, Vuejs, тестирования в spring boot.

### How do I get set up? ###

В случае если вы используюте intellij Idea будет достаточно просто сделать checkout from VCS.

В пакете \src\main\resources в файле application.properties изменить datasource т.к. я использовал PostgreSql 
запущенный локально, например убрать коментарии с emebeded H2 и удалить в POM.xml @scope test c H2 зависимости.

Также для запуска клиентского приложения необходим NPM, необходимо перейти в терминале в папку "\frontend\my-frontend" 
и выполнить установку всех зависимостей командой "npm install", зетем запустить девелоперский сервер командой "npm run dev"

### Contribution guidelines ###

*Буду рад критике, уточнениям, коректировкам
