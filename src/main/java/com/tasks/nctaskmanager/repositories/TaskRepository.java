package com.tasks.nctaskmanager.repositories;

import com.tasks.nctaskmanager.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("select t from Task t where t.alertDate =?1")
    List<Task> getTasksByAlertDateTime(LocalDate alertDate);

    @Modifying
    @Query("UPDATE Task t set t.Completed=?2   where t.id=?1")
    void setStatusForTaskByID(Long id, Boolean value);

    @Modifying
    @Query("UPDATE Task t set t.alertDate=?2   where t.id=?1")
    void delayTaskById(Long id, LocalDate newDate);

}
