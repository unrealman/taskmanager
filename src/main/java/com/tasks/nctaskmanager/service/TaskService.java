package com.tasks.nctaskmanager.service;

import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.model.User;
import com.tasks.nctaskmanager.exception.TaskNotFoundException;
import com.tasks.nctaskmanager.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
@Transactional
public class TaskService {
    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SimpMessagingTemplate template;

    private static final String MESSAGE = "no Task found with id:";

    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    public void insertTask(Task task) {

        // костыль такой своебразный из-за того что не сделай я его была бы ощибка EntityManager`a Detached Entity
        Set<User> usersFromRepo = new HashSet<>();
        for (User user : task.getUsers()) {
//            usersFromRepo.add(userService.findOne(user.getId()));
            usersFromRepo.add(userService.findUserByName(user.getUsername()));
        }
        task.setUsers(usersFromRepo);

        taskRepository.save(task);

        notifyUsers(task.getUsers());
    }

    public void updateTask(Task task) {
        taskRepository.save(task);
        Set<User> usersFromRepo = new HashSet<>();
        for (User user : task.getUsers()) {
            usersFromRepo.add(userService.findOne(user.getId()));
        }
        task.setUsers(usersFromRepo);
        taskRepository.save(task);

        notifyUsers(task.getUsers());

    }


    public List<Task> getTasksForSelectedDate(LocalDate alertDate) {
        return taskRepository.getTasksByAlertDateTime(alertDate);
    }

    @Transactional(rollbackFor = {TaskNotFoundException.class})
    public void removeTaskById(Long id) throws TaskNotFoundException {
        if (taskRepository.exists(id)) {
            Set<User> users = new HashSet<>(taskRepository.findOne(id).getUsers());
            taskRepository.delete(id);

            notifyUsers(users);
        } else {
            throw new TaskNotFoundException(MESSAGE + id);
        }
    }

    @Transactional(rollbackFor = {TaskNotFoundException.class})
    public void setStatusForTaskByID(Long id) throws TaskNotFoundException {

        if (taskRepository.exists(id)) {
            taskRepository.setStatusForTaskByID(id, !taskRepository.findOne(id).getCompleted());

            notifyUsers(taskRepository.findOne(id).getUsers());
        } else {
            throw new TaskNotFoundException(MESSAGE + id);
        }
    }

    @Transactional(rollbackFor = {TaskNotFoundException.class})
    public void delayTaskByID(Long id) throws TaskNotFoundException {
        if (taskRepository.exists(id)) {
            taskRepository.delayTaskById(id, taskRepository.findOne(id).getAlertDate().plusDays(1));

            notifyUsers(taskRepository.findOne(id).getUsers());
        } else {
            throw new TaskNotFoundException(MESSAGE + id);
        }
    }

    public List<Task> getTasksForSpecificUser(String user) {
        return taskRepository.findAll().stream()
                .filter(task -> task.getUsers()
                        .contains(userService.findUserByName(user)))
                .collect(Collectors.toList());
    }

    public Task getTask(Long id) {
        return taskRepository.findOne(id);
    }


    private void notifyUsers(Set<User> users) {
        Set<User> usersToNotify = new HashSet<>();
        usersToNotify.addAll(users);
        usersToNotify.addAll(userService.getUsersWithAdminRole());
        for (User user : usersToNotify) {
            //   сообщение отправляется подпищикам на "/user/{username}/notify"
            template.convertAndSendToUser(user.getUsername(), "/notify", "there are some changes in your tasks");
        }
    }

    public void deleteALl() {
        Set<User> users = new HashSet<>();
        for (Task task : taskRepository.findAll()) {
            users.addAll(task.getUsers());
        }
        taskRepository.deleteAll();
        notifyUsers(users);
    }
}
