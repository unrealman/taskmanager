package com.tasks.nctaskmanager.service;

import com.tasks.nctaskmanager.model.CustomUserDetails;
import com.tasks.nctaskmanager.model.User;
import com.tasks.nctaskmanager.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> usersOptional = Optional.ofNullable(userRepository.findByUsername(s));

        usersOptional
                .orElseThrow(() -> new UsernameNotFoundException("User with given username not found"));
        return usersOptional
                .map(CustomUserDetails::new)
                .get();
    }
}
