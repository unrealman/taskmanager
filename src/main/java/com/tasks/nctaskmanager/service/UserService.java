package com.tasks.nctaskmanager.service;

import com.tasks.nctaskmanager.model.Role;
import com.tasks.nctaskmanager.repositories.UserRepository;
import com.tasks.nctaskmanager.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository repo;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @PostConstruct
    private void ifUserRepositoryIsNull() {
        if (repo.count() == 0L) {
            repo.save(new User("admin", passwordEncoder.encode("admin"), Arrays.asList(new Role("USER"), new Role("ACTUATOR"), new Role("ADMIN"))));
            repo.save(new User("user", passwordEncoder.encode("user"), Arrays.asList(new Role("USER"), new Role("ACTUATOR"))));
        }
    }

    public User save(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        repo.save(user);
        return user;
    }

    public User findUserByName(String name) {
        return repo.findByUsername(name);
    }

    public List<User> getAllUsers() {
        return repo.findAll();
    }

    public List<User> getUsersWithAdminRole() {

        List<User> result = new ArrayList<>();

        for (User user : repo.findAll()) {
            for (Role role : user.getRoles()) {
                if ("ADMIN".equals(role.getName())) {
                    result.add(user);
                }
            }

        }
        return result;
    }


    public User findOne(Long id) {
        return repo.findOne(id);
    }

    public void deleteAll() {
        repo.deleteAll();
    }
}