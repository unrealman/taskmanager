package com.tasks.nctaskmanager.controller;

import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.service.TaskService;
import com.tasks.nctaskmanager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class WebSocketController {


    @Autowired
    private TaskService taskService;
    @Autowired
    private UserService userService;

    @SendTo("/tasksWS")
    @MessageMapping("/send/task")
    public List<Task> createTask(Task task) throws Exception {
        taskService.insertTask(task);
        return taskService.getAllTasks();
    }

    @SendTo("/tasksWS")
    @MessageMapping("/delete/task")
    public List<Task> deleteTask(Long id) throws Exception {
        taskService.removeTaskById(id);
        return taskService.getAllTasks();
    }

    @SendTo("/tasksWS")
    @MessageMapping("/update/task")
    public List<Task> updateTaskByid(Task task) throws Exception {
        taskService.updateTask(task);
        return taskService.getAllTasks();
    }

}