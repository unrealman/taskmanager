package com.tasks.nctaskmanager.controller;

import com.tasks.nctaskmanager.dto.TaskCreationDto;
import com.tasks.nctaskmanager.dto.TaskDto;
import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.exception.TaskNotFoundException;
import com.tasks.nctaskmanager.service.TaskService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ModelMapper modelMapper;


    @RequestMapping(value = "/api/tasks", method = RequestMethod.GET)
    public List<TaskDto> tasks() {
        List<Task> tasks = taskService.getAllTasks();
        return tasks.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/api/tasks/{user}", method = RequestMethod.GET)
    public List<TaskDto> getTasksForSpecificUser(@PathVariable String user) {
        List<Task> tasks = taskService.getTasksForSpecificUser(user);
        return tasks.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }


    @RequestMapping(value = "/api/task", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void createTask(@RequestBody TaskCreationDto taskCreationDto) {
        Task entityTask = convertToEntity(taskCreationDto);
        taskService.insertTask(entityTask);
    }


    @RequestMapping(value = "/api/task/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTaskById(@PathVariable("id") Long id) throws TaskNotFoundException {
        taskService.removeTaskById(id);

    }

    @RequestMapping(value = "/api/task/{id}/status", method = RequestMethod.PUT)
    public ResponseEntity<?> setStatusForTaskByID(@PathVariable("id") Long id) throws TaskNotFoundException {

        taskService.setStatusForTaskByID(id);

        return new ResponseEntity<>(new Object() {
            String message = "Task's status has been changed";

            public String getMessage() {
                return message;
            }
        }, HttpStatus.OK);
    }

    @RequestMapping(value = "/api/delayTask/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> delayTaskByID(@PathVariable("id") Long id) throws TaskNotFoundException {
        taskService.delayTaskByID(id);

        return new ResponseEntity<>(new Object() {
            String message = "Task has been delayed";

            public String getMessage() {
                return message;
            }
        }, HttpStatus.OK);
    }

    private TaskDto convertToDto(Task task) {
        return modelMapper.map(task, TaskDto.class);
    }

    private Task convertToEntity(TaskCreationDto taskDto) throws ParseException {
        return modelMapper.map(taskDto, Task.class);
    }

    @ExceptionHandler(TaskNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<?>  handleTodoNotFoundException(TaskNotFoundException ex) {
        return new ResponseEntity<>(new Object() {
            String message = "Task with given Id not Found";

            public String getMessage() {
                return message;
            }
        }, HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/api/tasks", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAll() {
        taskService.deleteALl();
    }
}
