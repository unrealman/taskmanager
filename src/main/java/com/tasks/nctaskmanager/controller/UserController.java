package com.tasks.nctaskmanager.controller;

import com.tasks.nctaskmanager.dto.UserCreationDTO;
import com.tasks.nctaskmanager.model.User;
import com.tasks.nctaskmanager.dto.UserDTO;
import com.tasks.nctaskmanager.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @RequestMapping(value = "/users")
    public List<UserDTO> getAllUsers() {
        List<User> users = userService.getAllUsers();
        return users.stream()
                .map(user -> convertToDto(user))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public UserDTO registerUser(@RequestBody UserCreationDTO userDTO) {
        User user = convertToEntity(userDTO);
        User userCreated = userService.save(user);
        return convertToDto(userCreated);
    }

    @RequestMapping("/exit")
    public void exit(HttpServletRequest request, HttpServletResponse response) {
        new SecurityContextLogoutHandler().logout(request, null, null);
    }


    @GetMapping(value = "/getUserRoles")
    public Collection<? extends GrantedAuthority> getUserRoles() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities();
    }

    @RequestMapping("/singOut")
    public void singOut() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private UserDTO convertToDto(User user) {
        UserDTO userDto = modelMapper.map(user, UserDTO.class);
        return userDto;
    }

    private User convertToEntity(UserCreationDTO userDto) throws ParseException {
        User user = modelMapper.map(userDto, User.class);
        return user;
    }
}
