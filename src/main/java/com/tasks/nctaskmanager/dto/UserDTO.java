package com.tasks.nctaskmanager.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class UserDTO {
    @Id
    @GeneratedValue
    private long id;

    private String username;

    public UserDTO(long id, String username) {
        this.id = id;
        this.username = username;
    }

    public UserDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

