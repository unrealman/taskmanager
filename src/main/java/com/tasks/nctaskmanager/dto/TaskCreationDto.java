package com.tasks.nctaskmanager.dto;

import com.tasks.nctaskmanager.model.User;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class TaskCreationDto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String title;
    private String description;

    private LocalDate alertDate;
    private Boolean Completed;

    private Set<UserDTO> users = new HashSet<>();

    public TaskCreationDto(String title, String description, LocalDate alertDate, Boolean completed, Set<UserDTO> users) {
        this.title = title;
        this.description = description;
        this.alertDate = alertDate;
        Completed = completed;
        this.users = users;
    }

    public TaskCreationDto(String title, String description, LocalDate alertDate, Boolean completed) {
        this.title = title;
        this.description = description;
        this.alertDate = alertDate;
        Completed = completed;
    }

    public TaskCreationDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getAlertDate() {
        return alertDate;
    }

    public void setAlertDate(LocalDate alertDate) {
        this.alertDate = alertDate;
    }

    public Boolean getCompleted() {
        return Completed;
    }

    public void setCompleted(Boolean completed) {
        Completed = completed;
    }

    public Set<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDTO> users) {
        this.users = users;
    }
}
