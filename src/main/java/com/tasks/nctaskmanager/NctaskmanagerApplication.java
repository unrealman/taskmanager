package com.tasks.nctaskmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

//for jsr310 java8 java.Time
@EntityScan(
        basePackageClasses = {NctaskmanagerApplication.class, Jsr310JpaConverters.class}
)

@SpringBootApplication
public class NctaskmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NctaskmanagerApplication.class, args);
    }

}
