package com.tasks.nctaskmanager.controller;

import com.tasks.nctaskmanager.NctaskmanagerApplication;
import com.tasks.nctaskmanager.model.Role;
import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.model.User;
import com.tasks.nctaskmanager.service.TaskService;
import com.tasks.nctaskmanager.service.UserService;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = NctaskmanagerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integrationtest.properties")
public class TaskControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        //не нужно т.к. я проверяю что UserRepo рипозиторий что-то содержит иначе создает 2 захардкоженых
        //userService.save(new User("admin", "admin", Collections.singletonList(new Role("ADMIN"))));
        Task task = new Task(
                "test", "test",
                LocalDate.of(2018, 3, 3), false);

        Set<User> users = new HashSet<>();
        User admin = new User();

        admin.setUsername("admin");
        users.add(admin);
        task.setUsers(users);
        taskService.insertTask(task);
    }

    @After
    public void tearDown() throws Exception {
        //  userService.deleteAll();
        taskService.deleteALl();
    }

    @Test
    public void checkItPossibleToObtainAccessToken() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", "admin");
        params.add("password", "admin");


        mockMvc
                .perform(post("/oauth/token")
                        .params(params)
                        .with(httpBasic("my-trusted-client", "secret"))
                        .accept("application/json;charset=UTF-8")
                        .params(params))
                .andExpect(status().isOk());

    }

    @Test
    public void tasks() throws Exception {
        // получить все таски из бд
        mockMvc
                .perform(get("/api/tasks")
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].title", is("test")));
    }

    @Test
    public void getTasksForSpecificUser() throws Exception {

        mockMvc
                .perform(get("/api/tasks/{name}", "admin")
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].title", is("test")));

    }

    @Test
    public void createTask() throws Exception {
        //постим с пустым телом
        mockMvc
                .perform(post("/api/task")
                        .header("Authorization", "Bearer " + obtainToken())
                        .content("")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        mockMvc
                .perform(post("/api/task")
                        .header("Authorization", "Bearer " + obtainToken())
                        .content("{\"title\": 123, \"description\": \"123\", \"alertDate\": \"2018-02-15\", \"completed\": false, \"users\":[{\"username\":\"admin\"}]}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void deleteTaskById() throws Exception {

        Long id = taskService.getAllTasks().get(0).getId();
        mockMvc
                .perform(delete("/api/task/{id}", id)
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteByIdWhenTaskIsNotFound() throws Exception {

        //удаление оповещения с заведомо не существующим id
        mockMvc
                .perform(delete("/api/task/{id}", 9999L)
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void setStatusForTaskByID() throws Exception {
        Long id = taskService.getAllTasks().get(0).getId();
        mockMvc
                .perform(put("/api/task/{id}/status", id)
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        //модификация оповещения с заведомо не существующим id
        mockMvc
                .perform(put("/api/task/{id}/status", 9999L)
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void delayTaskByID() throws Exception {

        Long id = taskService.getAllTasks().get(0).getId();

        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("access_token", obtainToken());
        // спросить как правильнее .header или .param ?
        mockMvc
                .perform(put("/api/delayTask/{id}", id)
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        //модификация оповещения с заведомо не существующим id
        mockMvc
                .perform(put("/api/delayTask/{id}", 9999L)
                        .header("Authorization", "Bearer " + obtainToken())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
        //заведомо не верный access_token
        mockMvc
                .perform(put("/api/delayTask/{id}", 9999L)
                        .header("Authorization", "Bearer " + "123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    /*@Test
    public void whenIncorrectTaskDtoIsSend() {
        //{"title": 123, "description": "123", "alertDate": "2018-02-15", "completed": false, "users":[{"username":"admin"}]}

    }*/

    public String obtainToken() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
//        params.add("client_id", "my-trusted-client");
        params.add("username", "admin");
        params.add("password", "admin");


        ResultActions actions = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic("my-trusted-client", "secret"))
                .accept("application/json;charset=UTF-8")
                .params(params))
                .andExpect(status().isOk());

        String resultString = actions.andReturn().getResponse().getContentAsString();


        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();

    }
}
