package com.tasks.nctaskmanager;

import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@JsonTest
public class JsonSerializationDeserializationTests {

    @Autowired
    private JacksonTester<Task> json;

    private static final LocalDate localDate = LocalDate.of(2018, 2, 15);


    @Test
    public void testSerialization() throws IOException {
        Task task = new Task("test", "test",
                localDate, false);

        assertThat(json.write(task)).hasJsonPathStringValue("@.title");
        assertThat(json.write(task)).extractingJsonPathStringValue("@.title")
                .isEqualTo("test");

    }

    @Test
    public void testDeserialization() throws IOException {
        String content = "{\"title\": 123, \"description\": \"123\", \"alertDate\": \"2018-02-15\", \"completed\": false, \"users\":[{\"username\":\"admin\"}]}";
        Task task = new Task("123", "123",
                localDate, false);
        User user = new User();
        user.setUsername("admin");
        Set<User> users = new HashSet<>();
        users.add(user);
        task.setUsers(users);

        //странно почему spring.io приводили в пример такую проверку ибо получаются
        // 2 ссылки на разные объекты? или это было при условии что я скажем переопределил метод equals унаследованный от Object?
/*
        assertThat(json.parse(content))
                .isEqualTo(task);*/
        assertThat(json.parseObject(content).getTitle()).isEqualTo("123");

        assertThat(json.parseObject(content).getUsers().size()).isEqualTo(1);



    }
}
