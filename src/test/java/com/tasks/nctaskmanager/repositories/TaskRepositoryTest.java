package com.tasks.nctaskmanager.repositories;

import com.tasks.nctaskmanager.model.Role;
import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.model.User;
import com.tasks.nctaskmanager.service.TaskService;
import com.tasks.nctaskmanager.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@MockBean(TaskService.class)
@MockBean(UserService.class)
public class TaskRepositoryTest {

    private static final LocalDate localDate = LocalDate.of(2018, 2, 23);
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TaskRepository taskRepository;

    @Before
    public void setUp() throws Exception {

        Task task = new Task("test", "test", localDate, false);
        Set<User> users = new HashSet<>();
        users.add(new User("admin", "admin", Collections.singletonList(new Role("ROLE_ADMIN"))));
        task.setUsers(users);
//        entityManager.persist(task);
//        entityManager.flush();
        taskRepository.save(task);
        entityManager.flush();
    }

    @After
    public void tearDown() throws Exception {
        taskRepository.deleteAll();
        entityManager.flush();
    }

    @Test
    public void setStatusForTaskByID() {
        //when
        Long id = taskRepository.findAll().get(0).getId();
        taskRepository.setStatusForTaskByID(id, true);

        entityManager.refresh(taskRepository.findOne(id));

//        entityManager.flush();
        //then
        assertThat(taskRepository.findOne(id).getCompleted())
                .isEqualTo(true);
    }

    @Test
    public void delayTaskById() {

        LocalDate nextDay = localDate;
        nextDay = nextDay.plusDays(1);
        //when
        Long id = taskRepository.findAll().get(0).getId();
        taskRepository.delayTaskById(id, nextDay);

        //Я не понимаю почему даты не совпадают, я же сохраняю изменения втф!!
        //entityManager.persistAndFlush(taskRepository.findOne(id));
        entityManager.refresh(taskRepository.findOne(id));
        entityManager.flush();
        //then
        assertThat(taskRepository.findOne(id).getAlertDate())
                .isEqualTo(nextDay);

    }
}