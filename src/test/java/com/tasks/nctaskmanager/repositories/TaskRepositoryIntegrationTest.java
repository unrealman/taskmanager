package com.tasks.nctaskmanager.repositories;

import com.tasks.nctaskmanager.model.Role;
import com.tasks.nctaskmanager.model.Task;
import com.tasks.nctaskmanager.model.User;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserRepository userRepository;
/*

    @PersistenceContext
    private EntityManager entityManager;
*/

    private static final LocalDate localDate = LocalDate.of(2018, 3, 3);

    @Before
    public void setUp() throws Exception {
        Task task = new Task("test", "test",
                localDate, false);
/*        Set<User> users = new HashSet<>();
        users.add(userRepository.findByUsername("admin"));*/
        task.setUsers(new HashSet<>(userRepository.findAll()));
//        task.setUsers(users);
        taskRepository.save(task);
//        entityManager.flush();
    }

    @Test
    public void setStatusForTask() {

        Long id = taskRepository.findAll().get(0).getId();
        //when
        taskRepository.setStatusForTaskByID(id, true);

        entityManager.refresh(taskRepository.findOne(id));
//        sessionFactory.getCurrentSession().flush();
//        entityManager.refresh(id);
        //then
        assertThat(taskRepository.findOne(id).getCompleted())
                .isEqualTo(true);
    }

    @Test
    public void delayTask() {

        Long id = taskRepository.findAll().get(0).getId();

        taskRepository.delayTaskById(id, localDate.plusDays(1));
//        sessionFactory.getCurrentSession().flush();
//        entityManager.flush();
        entityManager.refresh(taskRepository.findOne(id));

        assertThat(taskRepository.findOne(id).getAlertDate())
                .isEqualTo(localDate.plusDays(1));

    }
}