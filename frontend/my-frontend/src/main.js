import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import VueRouter from 'vue-router'
import Login from './components/login.vue'
import ListTasks from "./components/listTasks.vue";
import {store} from './store/index.js'


Vue.use(BootstrapVue);
Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: Login
    },
    {
        path: '/listTasks',
        component: ListTasks
    }
];

export const router = new VueRouter({
    routes,
    mode: 'history'
});


const myVue = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
});
