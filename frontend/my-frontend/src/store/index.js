import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';
import * as moment from "moment";
import {router} from "../main.js";
import createPersistedState from 'vuex-persistedstate';
import SockJS from "sockjs-client";
import Stomp from "webstomp-client";

Vue.use(Vuex);


//перехватчик запросов, добавляет access token ко всем запросам
Axios.interceptors.request.use(function (config) {
    const token = store.state.user.token;

    if (token != null) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
}, function (error) {
    // console.log(error + "    error");
    return Promise.reject(error);
});
//перехватчик ответов, проверяет если HTTP статус ответа равен 401 то отправляет на страницу логина
Axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.data.message === 'invalid_token') {
        router.push('/');
    }
    if (error.response.status === 401) {
        router.push('/');
    }

    if (error.response.data.message === 'Task with given Id not Found') {
        store.dispatch('getTasksForSpecificUser');
    }
    return Promise.reject(error);
});

export const store = new Vuex.Store({
    state: {
        user: {
            username: '',
            roles: [{authority: 'temp'}],
            token: '',
            isAuthorized: false
        },
        allTasks: {
            id: '',
            title: '',
            description: '',
            alertDate: '',
            completed: false
        },
        tempTask: {
            title: 'zz',
            description: 'zzz',
            alertDate: '2018-02-20',
            completed: false
        },
        socket: {
            connected: false
        },
        stompClient: {},
        users: {}
    },
    getters: {
        hasAdminRole(state) {
            //spring security автоматически добавляет ко всем ролям "Role_", поэтому необходимо искать имеено эту роль
            return state.user.roles.some(role => role.authority === 'ROLE_ADMIN')
        },
        isAuthorized(state) {
            return state.user.isAuthorized;
        },
        getUsername(state) {
            return state.user.username;
        },
        getTodayTasks(state) {
            const todayString = moment().format('YYYY-MM-DD');
            return state.allTasks.filter(task => task.alertDate === todayString);
        },
        getStoredToken(state) {
            return state.user.token;
        },
        getTasksForSpecificDate: (state) =>
            (providedDate) => {
                return state.allTasks.filter((task) => {
                    return task.alertDate === providedDate;
                });

            },

        getListOfUsers: (state) => {
            return state.users;
        }
    },
    mutations: {
        setUser(state, payload) {
            state.user.token = payload.token;
            state.user.username = payload.username;
            state.user.isAuthorized = true;
        },
        setRoles(state, payload) {
            state.user.roles = payload
        },
        logout(state) {
            state.user.token = '';
            state.user.username = '';
            state.user.isAuthorized = false;
            state.user.roles = [];
        },
        getAllTasks(state, data) {
            state.allTasks = data
        },
        getListOfUsers(state, data) {
            state.users = data
        },
        setWebSocketStatus(state, status) {
            // state.connected = status;
            state.socket.connected = status;
        }
    },
    actions: {
        getAllTasks(context) {
            //первоначальное получение событий при старте
            context.dispatch('getTasksForSpecificUser', context);

            //connect WebSocket
            context.dispatch('connect', context);
        },
        getTasksForSpecificUser(context) {

            let url = store.getters.hasAdminRole ? '/api/tasks/' : '/api/tasks/' + context.state.user.username;
            Axios({
                method: 'get',
                url: url,
            }).then(response => {
                context.commit('getAllTasks', response.data)
            });

        },
        connect(context) {
            this.socket = new SockJS("http://localhost:7070/socket");
            /* this.socket.onclose=()=>{
                 this.socket = new SockJS("http://localhost:7070/socket");
             };
 */
            this.stompClient = Stomp.over(this.socket);
            this.stompClient.connect(
                {},
                frame => {
                    context.commit('setWebSocketStatus', true);
                    console.log(frame);
                    //сейчас вебсокет используется только для уведомлений в
                    // комментариях остался код который был для реализации когда все действия осуществлялись через вебсокет
                    /*
                        this.stompClient.subscribe("/tasksWS", tick => {
                            console.log(tick);
                            context.commit('getAllTasks', JSON.parse(tick.body));
                        });*/

                    this.stompClient.subscribe("/user/" + context.state.user.username + "/notify", tick => {
                        console.log(tick + " -- Got Response from server");
                        //без задержки не успевает завершиться транзакция ^^
                        setTimeout(() => {
                            context.dispatch('getTasksForSpecificUser')
                        }, 400);
                    });
                },
                error => {
                    console.log(error);
                    context.commit('setWebSocketStatus', false);
                }
            );
        },

        disconnect(context) {
            this.socket.close();
            if (this.stompClient) {
                this.stompClient.disconnect();
            }
            // console.log(context.state.socket.connected + "------ context.state.connected");
            context.commit('setWebSocketStatus', false);
            Axios({
                    method: 'post',
                    url: '/api/singOut'
                }
            ).then(response => {
                context.commit('logout')
            });
            // context.commit('logout');
        },
        //замена действия ибо реализованно через веб сокет (((
        /*sendTask(context, task) {
            console.log("Send task:" + context.state.tempTask);
            if (this.stompClient && this.stompClient.connected) {
                // const msg = {title: '123', description: '123', alertDate: '2018-02-15', completed: false, users[{username:'admin'}]};
                this.stompClient.send("/app/send/task", JSON.stringify(task), {});
            }
        },*/

        sendTask(context, task) {
            Axios({
                method: 'post',
                url: '/api/task',
                data: task,
                headers: {"Content-type": "application/json"}
            });
        },
        //замена действия ибо реализованно через веб сокет (((
        /*     updateTask(context, task) {
                 if (this.stompClient && this.stompClient.connected) {
                     this.stompClient.send("/app/update/task", JSON.stringify(task), {});
                 }
             },
     */
        updateTask(context, task) {
            Axios({
                method: 'post',
                url: '/api/task',
                data: task,
                headers: {"Content-type": "application/json"}
            }).then(() => {
            }).catch((error) => {
            });
        },


        //замена действия ибо реализованно через веб сокет (((
        /*deleteTask(context, id) {
            if (this.stompClient && this.stompClient.connected) {
                // this.stompClient.send("/app/delete/task", JSON.stringify({'id': id}), {});
                this.stompClient.send("/app/delete/task", id, {});
            }
        }
    */
        deleteTask(context, id) {
            Axios({
                method: 'delete',
                //"/api/task/{id}"
                url: '/api/task/' + id,
            }).then(() => {
            }).catch((error) => {
                console.log(error);
            });
        },
        getListOfUsers(context) {
            Axios({
                method: 'get',
                url: '/api/users'
            }).then(response => {
                context.commit('getListOfUsers', response.data)
            }).catch((error) => {
            });

        }
    },
    plugins: [createPersistedState()]
});
